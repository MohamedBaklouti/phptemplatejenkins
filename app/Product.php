<?php
/**
 * Created by PhpStorm.
 * User: medbak
 * Date: 16/07/18
 * Time: 15:30
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name', 'product_description',
    ];
}