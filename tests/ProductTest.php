<?php


class ProductTest extends TestCase
{
    /**
     * /products [GET]
     */
    public function testShouldReturnAllProducts(){
        $this->get("products", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'product_name',
                    'product_description',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ],
            'meta' => [
                '*' => [
                    'total',
                    'count',
                    'per_page',
                    'current_page',
                    'total_pages',
                    'links',
                ]
            ]
        ]);

    }
    /**
     * /products/id [GET]
     */
    public function testShouldReturnProduct(){
        $this->get("products/2", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'product_name',
                    'product_description',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]
        );

    }
    /**
     * /products [POST]
     */
    public function testShouldCreateProduct(){
        $parameters = [
            'product_name' => 'Infinix',
            'product_description' => 'NOTE 4 5.7-Inch IPS LCD (3GB, 32GB ROM) Android 7.0 ',
        ];
        $this->post("products", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'product_name',
                    'product_description',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]
        );

    }

    /*
        failed creation
    */
    public function testShouldCreateProduct1(){
        $parameters = [
            'product_name' => 'Infinix',

        ];
        $this->post("products", $parameters, []);
        $this->seeStatusCode(422);
        ;

    }

    /**
     * /products/id [PUT]
     */
    public function testShouldUpdateProduct(){
        $parameters = [
            'product_name' => 'aaaaa',
            'product_description' => 'Champagne Gold, 13M AF + 8M FF 4G Smartphone',
        ];
        $this->put("products/70", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'product_name',
                    'product_description',
                    'created_at',
                    'updated_at',
                    'links'
                ]
            ]
        );
    }
    /**
     * /products/id [DELETE]
     */
   /* public function testShouldDeleteProduct(){

        $this->delete("products/64", [], []);
        $this->seeStatusCode(410);
        $this->seeJsonStructure([
            'status',
            'message'
        ]);
    }*/

    public function testDatabase()
    {
        $this->seeInDatabase('products',['product_name'=>'aaaaa']);
    }

    public function testFunction()
    {
        $response = $this->get('/products');
       // $this->assertEquals(200,$response);
        //$response->assertStatus(200);
        $response->assertResponseOk();


    }

    public function testBasicTest()
    {
        $this->assertTrue(true);
    }
}
